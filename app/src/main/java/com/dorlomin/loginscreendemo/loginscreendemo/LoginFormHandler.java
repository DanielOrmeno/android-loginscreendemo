/***************************************************************************************************
 |   File: LoginActivity.java
 |   Project: LoginScreenDemo
 |
 |   Description: - TODO
 |   Copyright (c) Daniel Ormeno. All rights reserved.
 **************************************************************************************************/

package com.dorlomin.loginscreendemo.loginscreendemo;

import android.content.Context;
import android.widget.Toast;

public class LoginFormHandler {

    // ====================     INSTANCE VARIABLES / PROPERTIES      =============================//

    private Context APP_CONTEXT;
    private int TOAST_DURATION;

    //- LOGIN FORM FIELDS
    private String email;
    private String password;

    //- Validates user input
    private boolean ValidCredentials;

    // ====================     CLASS CONSTRUCTORS  ==============================================//

    public LoginFormHandler(Context appContext, String emailInput, String passInput) {

        //- Toast defaults
        this.APP_CONTEXT = appContext;
        this.TOAST_DURATION = Toast.LENGTH_SHORT;

        //- Login/Register form fields
        this.email = emailInput;
        this.password = passInput;

        this.ValidCredentials = validateForm();
    }

    // ====================     ACCESSORS / MUTATORS  ============================================//

    public boolean isInputValid () {
        return this.ValidCredentials;
    }

    // ====================     CLASS METHODS  ===================================================//

    /***********************************************************************************************
     METHOD NAME: validateForm
     INPUT PARAMETERS: None
     RETURNS: Boolean

     OBSERVATIONS: Validates user input - Checks for empty fields, email format and invalid password
     length
     **********************************************************************************************/
    private boolean validateForm () {

        // - Boolean Result
        boolean validForm = true;

        if (!validateEmptyFields()||!validateCredentials()){
            validForm = !validForm;
        }

        return validForm;
    }

    /***********************************************************************************************
     METHOD NAME: validateEmptyFields
     INPUT PARAMETERS: None
     RETURNS: Boolean

     OBSERVATIONS: Returns true if required form fields are not empty, returns false otherwise
     **********************************************************************************************/
    private boolean validateEmptyFields () {

        //- Boolean result
        boolean isValid = true;

        //- Empty string for toast message.
        String toastText = new String();

        //- If email is empty append to string
        if (this.email.isEmpty()) {
            toastText += "Email";
        }

        //- If password is empty append appropriate text to string
        if (this.password.isEmpty()) {

            if (toastText.isEmpty()) {
                toastText +="Password";
            } else {
                toastText +=" and password";
            }
        }

        //- If toastText is not empty - show toast
        if (!toastText.isEmpty()) {

            toastText +=" can't be empty";

            //-Toast
            Toast toast = Toast.makeText(this.APP_CONTEXT, toastText, this.TOAST_DURATION);
            toast.show();

            isValid = false;
        }

        return isValid;
    } //- End OF validateEmptyFields

    /***********************************************************************************************
     METHOD NAME: validateCredentials
     INPUT PARAMETERS: None
     RETURNS: Boolean

     OBSERVATIONS: TODO
     **********************************************************************************************/
    private boolean validateCredentials() {

        //- Boolean result
        boolean isValid = true;

        if (!this.email.matches("[a-zA-Z0-9\\.]+@[a-zA-Z0-9\\-\\_\\.]+\\.[a-zA-Z0-9]{3}")){
            isValid = !isValid;

            //- Show Error - Toast
            Toast tst = Toast.makeText(this.APP_CONTEXT,"Invalid email format",this.TOAST_DURATION);
            tst.show();
        }

        return isValid;
    }//- END OF validateEmail



} //- END OF LoginFormHandler CLASS
