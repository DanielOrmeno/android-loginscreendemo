/***************************************************************************************************
 |   File: LoginActivity.java
 |   Project: LoginScreenDemo
 |
 |   Description: - TODO
 |   Copyright (c) Daniel Ormeno. All rights reserved.
 **************************************************************************************************/

package com.dorlomin.loginscreendemo.loginscreendemo;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.animation.ObjectAnimator;
import android.animation.AnimatorSet;
import android.view.animation.Animation;
import android.view.animation.AlphaAnimation;


public class LoginActivity extends ActionBarActivity {

    // =======================     INSTANCE VARIABLES / PROPERTIES      ==========================//

    //- Activity TAG for console logs.
    private static final String TAG = LoginActivity.class.getSimpleName();

    // - Reference to UI elements - User Input.
    private EditText emailField;
    private EditText passwordField;
    private Button submitButton;

    //- Reference to UI elements - Layouts
    private View credentialsLayout;
    private View logoLayout;
    private View progressDialog;


    // =======================     ACTIVITY METHODS      =========================================//

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        Log.i(TAG, "this works");

                // - Reference to UI elements - User Input.
        this.emailField = (EditText) findViewById(R.id.email_editText);
        this.passwordField = (EditText) findViewById(R.id.password_editText);
        this.submitButton = (Button) findViewById(R.id.submit_button);

        this.credentialsLayout = findViewById(R.id.credentials_layout);
        this.logoLayout = findViewById(R.id.login_logo);
        this.progressDialog = findViewById(R.id.spinner_layout);

        //- Listen to submit button onClick events.
        submitButton.setOnClickListener(new View.OnClickListener() {

            //- Get user input and handle any empty fields -
            public void onClick(View v) {

                //- Get user input
                String email = String.valueOf(emailField.getText());
                String pass = String.valueOf(passwordField.getText());

                //- Validate User Input
                Context APP_CONTXT = getApplicationContext();
                Boolean isInputValid = new LoginFormHandler(APP_CONTXT, email, pass).isInputValid();

                if (isInputValid){

                    //- Authenticate user with server
                    //- TODO
                    //AuthenticationTask userLoginTask = new AuthenticationTask();
                    //userLoginTask.execute(email, pass, this);

                    //- Animate view to show progress
                    showLoginProgress();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // =======================    CUSTOM  METHODS      ===========================================//
    /***********************************************************************************************
     METHOD NAME: showLoginProgress
     INPUT PARAMETERS: None
     RETURNS: void

     OBSERVATIONS: TODO
     **********************************************************************************************/
    private void showLoginProgress () {

        //- Animation
        Animation fadeOut = new AlphaAnimation(1,0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setStartOffset(0);
        fadeOut.setDuration(500);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //- Lower Logo View
                logoLayout.animate().translationY(200);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //- Remove login form from view
                credentialsLayout.setVisibility(View.GONE);

                //- Show progress dialog
                progressDialog.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        //- Animate Views.
        credentialsLayout.startAnimation(fadeOut);
    }

} //- End of LoginActivity -
