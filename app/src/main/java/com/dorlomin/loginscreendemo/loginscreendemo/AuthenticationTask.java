/***************************************************************************************************
 |   File: AuthenticationTask.java
 |   Project: LoginScreenDemo
 |
 |   Description: - TODO
 |   Copyright (c) Daniel Ormeno. All rights reserved.
 **************************************************************************************************/
package com.dorlomin.loginscreendemo.loginscreendemo;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

public class AuthenticationTask extends AsyncTask <Object, Boolean, Boolean> {

    LoginActivity loginActivity;

    @Override
    protected Boolean doInBackground (Object... params){

        //- Fetch User Credentials from params
        String email = (String) params[0];
        String pass = (String) params[1];

        //- Fetch Application context from Params
        this.loginActivity = (LoginActivity) params[2];

        Log.i("HERE", "Got " + email + " and " + pass);

        //count to 100000
        for (int i = 0; i<100000; i++){
            i++;
        }

        return true;
    }

    @Override
    protected void onPostExecute (final Boolean success) {

        //this.loginActivity.findViewById(R.id.spinner_layout).setVisibility(View.GONE);
        //this.loginActivity.findViewById(R.id.credentials_layout).setVisibility(View.VISIBLE);

        Log.i("HERE","On Post Execute called");

    }

}

